package net.halalaboos.io;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    public static List<String> readFile(File file) {
        List tempList = new ArrayList();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            for (String s; (s = reader.readLine()) != null; ) {
                tempList.add(s.trim());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return tempList;
    }
    

    public static void writeFile(File file, List<String> text) {
    	writeFile(file, text.toArray(new String[text.size()]));
    }

    public static void writeFile(File file, String[] text) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileWriter(file));
            for (String s : text) {
                writer.println(s);
                writer.flush();
            }
        } catch (Exception localException) {
        } finally {
            if (writer != null)
                writer.close();
        }
    }
}
