/**
 * 
 */
package net.halalaboos.io;

/**
 * @author Halalaboos
 *
 */
public class Convert {

	public static <T> Object convert(T type, String input) {
		try {
			// Long
			if (type instanceof Long) {
				return Long.parseLong(input);
				
				// Integer
			} else if (type instanceof Integer) {
				return Integer.parseInt(input);
				
				// Double
			} else if (type instanceof Double) {
				return Double.parseDouble(input);
				
				// Float
			}  else if (type instanceof Float) {
				return Float.parseFloat(input);
				
				// Byte
			} else if (type instanceof Byte) {
				return Byte.parseByte(input);
				
				// Boolean
			} else if (type instanceof Boolean) {
				return Boolean.parseBoolean(input);
			} else {
				return input;
			}
		} catch (Exception e) {
			return type;
		}
	}
	
}
