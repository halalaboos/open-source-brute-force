package net.halalaboos.io.config;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * @author Halalaboos
 * */
public abstract class Config <ID, VALUE> {

	protected final String name;
	
	protected final Map<ID, VALUE> map = new ConcurrentHashMap<ID, VALUE>();
	
	protected List<ConfigListener<ID, VALUE>> listeners = new CopyOnWriteArrayList<ConfigListener<ID, VALUE>>();
	
	public Config(String name) {
		this.name = name;
	}
	
	public void save(File file) throws Exception {
		this.save(new FileOutputStream(file));
	}
	
	/**
	 * Writes the data to the output stream.
	 * */
	public void save(OutputStream outputStream) throws Exception {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
		for (Entry<ID, VALUE> entry : entrySet()) {
			writer.write(write(entry.getKey(), entry.getValue()) + "\n");
		}
		writer.close();
	}
	
	public void load(File file) throws Exception {
		this.load(new FileInputStream(file));
	}
	
	/**
	 * Reads the data from the input stream.
	 * */
	public void load(InputStream inputStream) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		for (String s = null; (s = reader.readLine()) != null;) {
			parse(s);
		}
		reader.close();
	}
		
	protected abstract void parse(String line) throws Exception;
	
	protected abstract String write(ID id, VALUE value) throws Exception;

	public VALUE get(ID id) {
        return map.get(id);
    }
	
	public void put(ID id, VALUE value) {
		map.put(id, value);
		for (ConfigListener configListener : listeners) {
			try {
				configListener.onConfigUpdate(id, value);
			} catch (Exception e) {}
		}
	}

	public void clear() {
		map.clear();
		for (ConfigListener configListener : listeners) {
			try {
				configListener.onClear();
			} catch (Exception e) {}
		}
	}

	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return map.containsValue(value);
	}

	public Set<Entry<ID, VALUE>> entrySet() {
		return map.entrySet();
	}

	public int size() {
		return map.size();
	}
    
	public void addListener(ConfigListener configListener) {
		listeners.add(configListener);
	}
	
	public void removeListener(ConfigListener configListener) {
		listeners.remove(configListener);
	}
	
}
