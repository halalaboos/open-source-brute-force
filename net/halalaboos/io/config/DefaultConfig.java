package net.halalaboos.io.config;

import java.io.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class DefaultConfig extends Config<String, String> {
   
	private final File file;
	
	/**
	 * @param name
	 */
	public DefaultConfig(String name, File file) {
		super(name);
		this.file = file;
	}

	public void put(String id, Object value) {
		this.put(id, value.toString());
	}
	
    public boolean getBoolean(String identifier) {
        try {
            return Boolean.parseBoolean(get(identifier).toUpperCase());
        } catch (Exception e) {
            return false;
        }
    }

    public double getDouble(String identifier) {
        try {
            return Double.parseDouble(get(identifier));
        } catch (Exception e) {
            return 0.0D;
        }
    }

    public int getInt(String identifier) {
        try {
            return Integer.parseInt(get(identifier));
        } catch (Exception e) {
            return 0;
        }
    }

    public float getFloat(String identifier) {
        try {
            return Float.parseFloat(get(identifier));
        } catch (Exception e) {
            return 0.0F;
        }
    }

    public long getLong(String identifier) {
        try {
            return Long.parseLong(get(identifier));
        } catch (Exception e) {
            return (long) 0.0;
        }
    }
    
    public void save() {
    	try {
			this.save(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    public void load() {
    	load(false);
    }
    
    public void load(boolean cleanCurrentData) {
    	try {
        	if (cleanCurrentData)
        		map.clear();
			this.load(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

	/**
	 * @see net.halalaboos.io.config.Config#parse(java.lang.String)
	 */
	@Override
	protected void parse(String line) throws Exception {
		if (line != null && line.contains(":")) {
			String id = line.substring(0, line.indexOf(":"));
			String value = line.substring(line.indexOf(":") + 1);
			map.put(id, value);
		}
	}

	/**
	 * @see net.halalaboos.io.config.Config#write(java.lang.Object, java.lang.Object)
	 */
	@Override
	protected String write(String id, String value) throws Exception {
		return id + ":" + value;
	}

}
