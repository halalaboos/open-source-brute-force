/**
 * 
 */
package net.halalaboos.io.config;

import net.halalaboos.io.Convert;

/**
 * @author Halalaboos
 *
 */
public class DefaultConfigListener <T> implements ConfigListener<String, String> {
	
	private T value;
	
	private final String id;
	
	private final T defaultValue;
	
	public DefaultConfigListener(String id, T defaultValue) {
		this.id = id;
		this.defaultValue = defaultValue;
		value = defaultValue;
	}
	
	/**
	 * @see net.halalaboos.io.config.ConfigListener#onConfigUpdate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void onConfigUpdate(String id, String value) {
		if (id.equalsIgnoreCase(this.id)) {
			this.value = (T) Convert.convert(defaultValue, value);
		}
	}

	/**
	 * @see net.halalaboos.io.config.ConfigListener#onClear()
	 */
	@Override
	public void onClear() {
		value = defaultValue;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public T getDefaultValue() {
		return defaultValue;
	}

}
