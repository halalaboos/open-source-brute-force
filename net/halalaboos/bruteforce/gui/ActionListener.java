package net.halalaboos.bruteforce.gui;

import java.io.File;

import net.halalaboos.bruteforce.Bruteforcer;
import net.halalaboos.bruteforce.Settings;
import net.halalaboos.bruteforce.utils.WindowHelper;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ActionListener implements EventHandler<ActionEvent> {

	private Controller controller;
	
	public ActionListener(Controller controller) {
		this.controller = controller;
	}

	@Override
	public void handle(ActionEvent e) {
		if (e.getSource() == controller.getBegin()) {
			Bruteforcer.getSession().start();
		} else if (e.getSource() == controller.getPause()) {
			Bruteforcer.getSession().stop();
		} else if (e.getSource() == controller.getSelectPlugin()) {
			Settings.setPluginId(controller.getSelectedPluginId());
		} else if (e.getSource() == controller.getFindProxies()) {
			File proxies = WindowHelper.chooseFile("Select");
			if (proxies != null) {
				controller.getProxiesField().setText(proxies.getAbsolutePath());
				Bruteforcer.getPluginManager().getSelectedPlugin().getProxyManager().loadProxies(proxies);
			}
		} else if (e.getSource() == controller.getPluginFolder()) {
			Bruteforcer.getSysUtils().openFile(Bruteforcer.getPluginManager().getLoader().getPluginDir());
		} else if (e.getSource() == controller.getClear()) {
			controller.getLogArea().clear();
		} else if (e.getSource() == controller.getUseHome()) {
			Settings.setUseHome(!Settings.useHome());
			Settings.save();
		} else if (e.getSource() == controller.getRandomProxy()) {
			Settings.setRandomProxy(!Settings.isRandomProxy());
			Settings.save();
		}
	}
}
