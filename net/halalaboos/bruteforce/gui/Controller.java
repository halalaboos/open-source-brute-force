package net.halalaboos.bruteforce.gui;

import net.halalaboos.bruteforce.Bruteforcer;
import net.halalaboos.bruteforce.Settings;
import net.halalaboos.bruteforce.plugin.Plugin;
import net.halalaboos.bruteforce.plugin.PluginData;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class Controller {

	@FXML
	private Button pluginFolder, selectPlugin, findProxies, begin, pause, clear;
	
	@FXML
	private TextField proxiesField, threadField, delayField;
	
	@FXML
	private TextArea logArea;
	
	@FXML
	private ListView<PluginData> pluginList;
	
	@FXML
	private MenuBar menuBar;
	
	@FXML
	private ScrollPane pluginOptions;
	
	@FXML
	private CheckBox useHome, randomProxy;
	
	private KeyListener keyListener = new KeyListener(this);
	
	private ActionListener actionListener = new ActionListener(this);
	
	public void setup() {
		generateMenubarItems();
		
		begin.addEventHandler(ActionEvent.ACTION, actionListener);
		pause.addEventHandler(ActionEvent.ACTION, actionListener);
		pause.setDisable(true);
		begin.setDisable(true);
		selectPlugin.addEventHandler(ActionEvent.ACTION, actionListener);
		findProxies.addEventHandler(ActionEvent.ACTION, actionListener);
		pluginFolder.addEventHandler(ActionEvent.ACTION, actionListener);
		clear.addEventHandler(ActionEvent.ACTION, actionListener);
		useHome.addEventHandler(ActionEvent.ACTION, actionListener);
		randomProxy.addEventHandler(ActionEvent.ACTION, actionListener);
		pluginList.setOnMouseClicked(new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent event) {
	        	Settings.setPluginId(getSelectedPluginId());
			}
	    });
		
		threadField.addEventHandler(KeyEvent.KEY_RELEASED, keyListener);
		delayField.addEventHandler(KeyEvent.KEY_RELEASED, keyListener);
		updatePluginList();
		loadSettings();
	}
		
	private void generateMenubarItems() {
		menuBar.getMenus().clear();
		Menu settingsMenu = new Menu("Settings");
		menuBar.getMenus().add(settingsMenu);
	}
	
	/**
	 * Disables the plugin buttons / list.
	 * */
	public void disablePluginChanging() {
		if (pluginList != null && selectPlugin != null) {
			pluginList.setDisable(true);
			selectPlugin.setDisable(true);
		}
	}
	
	/**
	 * Enables the plugin buttons / list.
	 * */
	public void enablePluginChanging() {
		if (pluginList != null && selectPlugin != null) {
			pluginList.setDisable(false);
			selectPlugin.setDisable(false);
		}
	}
	
	/**
	 * Disables the data/proxy fields.
	 * */
	public void disableProxyField() {
		if (proxiesField != null && findProxies != null) {
			proxiesField.setDisable(true);
			findProxies.setDisable(true);
			randomProxy.setDisable(true);
			useHome.setDisable(true);
		}
	}
	
	/**
	 * Enables the data/proxy fields.
	 * */
	public void enableProxyField() {
		if (proxiesField != null && findProxies != null) {
			proxiesField.setDisable(false);
			findProxies.setDisable(false);
			randomProxy.setDisable(false);
			useHome.setDisable(false);
		}
	}
	
	public void disableBegin() {
		if (begin != null) {
			begin.setDisable(true);
		}
	}
	
	public void enableBegin() {
		if (begin != null) {
			begin.setDisable(false);
		}
	}
	
	public void disablePause() {
		if (pause != null) {
			pause.setDisable(true);
		}
	}
	
	public void enablePause() {
		if (pause != null) {
			pause.setDisable(false);
		}
	}
	
	public void loadPluginOptions(Plugin plugin) {
		if (plugin != null && pluginOptions != null) {
			if (plugin.getData().requiresProxies())
				enableProxyField();
			else
				disableProxyField();
			pluginOptions.setContent(plugin.getOptionsManager().generateOptions(pluginOptions));
		}
	}
	
	public void loadSettings() {
		if (Settings.getPluginId() != -1) {
			pluginList.getSelectionModel().select(Settings.getPluginId());
			enableBegin();
		} else {
			disableBegin();
		}
		
		if (Settings.getThreads() != -1)
			threadField.setText(Settings.getThreads() + "");
		else
			Settings.setThreads(1);
		
		delayField.setText(Settings.getDelay() + "");
		
		if (Settings.useHome())
			useHome.setSelected(true);
		
		if (Settings.isRandomProxy())
			randomProxy.setSelected(true);
		
		loadPluginOptions(Bruteforcer.getPluginManager().getSelectedPlugin());
	}
	
	public void updatePluginList() {
		if (pluginList != null) {
			ObservableList<PluginData> list = FXCollections.observableArrayList();
			list.addAll(Bruteforcer.getPluginManager().getPluginData());
			pluginList.setItems(list);
		}
	}
	
	public int getSelectedPluginId() {
		return pluginList.getSelectionModel().getSelectedIndex();
	}
	
	public void log(final String text) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				logArea.appendText(text + "\n");				
			}
		});
	}

	public Button getPluginFolder() {
		return pluginFolder;
	}

	public Button getSelectPlugin() {
		return selectPlugin;
	}

	public Button getFindProxies() {
		return findProxies;
	}

	public Button getBegin() {
		return begin;
	}

	public Button getPause() {
		return pause;
	}

	public TextField getProxiesField() {
		return proxiesField;
	}

	public TextField getThreadField() {
		return threadField;
	}

	public ListView getPluginList() {
		return pluginList;
	}

	public Button getClear() {
		return clear;
	}

	public TextArea getLogArea() {
		return logArea;
	}

	public ScrollPane getPluginOptions() {
		return pluginOptions;
	}

	public CheckBox getUseHome() {
		return useHome;
	}

	public TextField getDelayField() {
		return delayField;
	}

	public CheckBox getRandomProxy() {
		return randomProxy;
	}
	
}
