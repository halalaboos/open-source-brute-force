package net.halalaboos.bruteforce.gui;

import net.halalaboos.bruteforce.Settings;
import net.halalaboos.bruteforce.utils.NumberHelper;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class KeyListener implements EventHandler<KeyEvent> {

	private Controller controller;
	
	public KeyListener(Controller controller) {
		this.controller = controller;
	}

	@Override
	public void handle(KeyEvent e) {
		if (e.getSource() == controller.getThreadField()) {
			String text = (controller.getThreadField().getText());
			if (!text.isEmpty()) {
				if (NumberHelper.isInteger(text))
					Settings.setThreads(Integer.parseInt(text));
			}
		} else if (e.getSource() == controller.getDelayField()) {
			String text = (controller.getDelayField().getText());
			if (!text.isEmpty()) {
				if (NumberHelper.isFloat(text)) {
					Settings.setDelay(Float.parseFloat(text));
					Settings.save();
				}
			}
		}
	}

}
