package net.halalaboos.bruteforce.gui;

import java.io.IOException;

import javax.swing.UIManager;

import net.halalaboos.bruteforce.plugin.data.WorkerData;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
 

public final class Gui {

	private Stage stage;
	
	private Parent parent;
	
	private final Controller controller = new Controller();
	
	public Gui() {
		
	}

	public void setup() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void startDisplay(Stage stage) throws IOException {
		this.stage = stage;
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("window.fxml"));
		fxmlLoader.setController(controller);
		Parent root = (Parent) fxmlLoader.load();
		this.parent = root;
		controller.setup();
		
		stage.setTitle("Open-Source Bruteforce");
		final Scene scene = new Scene(root, 478, 412);
		stage.setResizable(false);
		stage.setScene(scene);
		stage.getIcons().add(new Image(this.getClass().getResourceAsStream("icon.png")));
		stage.show();
	}
	
	public Controller getController() {
		return controller;
	}

	public Stage getStage() {
		return stage;
	}

	public Parent getParent() {
		return parent;
	}
}
