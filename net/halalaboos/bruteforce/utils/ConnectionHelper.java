package net.halalaboos.bruteforce.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;

public final class ConnectionHelper {
	
	public static HttpURLConnection openAsFirefox(URL url, Proxy proxy) throws IOException {
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection(proxy);
		urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0");
		return urlConnection;
	}
	
}
