package net.halalaboos.bruteforce.utils;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import net.halalaboos.bruteforce.Bruteforcer;

public final class Logger {
	
	private static final DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
	
	private static final Calendar calendar = Calendar.getInstance();
	
	private Logger() {
		
	}
	
	public static void logErr(Exception exception) {
		log(exception.getMessage(), System.err);
	}
	
	public static void logErr(String message) {
		log("[Err]: " + message, System.err);
	}
	
	public static void log(String message, PrintStream stream) {
		String text = "[" + dateFormat.format(calendar.getTime()) + "] " +  message;
		stream.println(text);
	}
	
	public static void log(String prefix, String message) {
		log("[" + prefix + "] " + message);
	}

	public static void log(String message) {
		log(message, System.out);
	}
	
}
