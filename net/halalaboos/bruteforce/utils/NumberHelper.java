package net.halalaboos.bruteforce.utils;

public class NumberHelper {

	public static long factorial(long number) {
		if (number == 0) 
			return 1;
        return number * factorial(number - 1);
	}
	
	public static boolean isInteger(String line) {
		try {
			Integer.parseInt(line);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isFloat(String line) {
		try {
			Float.parseFloat(line);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
