/**
 * 
 */
package net.halalaboos.bruteforce.utils.reflection;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author Halalaboos
 *
 */
public final class ReflectionHelper {
	
	/**
     * Loads an instance of T from an existing jar file.
     *
     * @param jarFile
     *         The (Expected jar file) we're instantiating our instance of T from.
     * @param classPath
     *         The class path of the T we're attempting to instantiate.
     * @param args
     *         The arguments passed through the constructor of the object.
	 * @return 
     */
    public static <T> T loadInstance(File jarFile, String classPath, Object... args) throws Exception {
        ClassLoader loader = URLClassLoader.newInstance(new URL[] {jarFile.toURI().toURL()}, ReflectionHelper.class.getClassLoader());
        Class loadedClass = Class.forName(classPath, true, loader);
        return (T) loadedClass.getConstructor().newInstance(args);
    }
    
    /**
     * Replaces everything in the instance object that extends the replacement object.
     * @throws IllegalAccessException 
     * @throws IllegalArgumentException 
     * 
     * */
    public static boolean replaceInstance(Class clazz, Object instance, Object replacement) throws IllegalArgumentException, IllegalAccessException {
    	Field[] fields = clazz.getDeclaredFields();
    	boolean replaced = false;
    	for (Field field : fields) {
    		if (field.getType().isAssignableFrom(replacement.getClass())) {
        		field.setAccessible(true);
    			field.set(instance, replacement);
    			replaced = true;
    		}
    	}
    	return replaced;
    }
    
}
