package net.halalaboos.bruteforce.utils;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ListHelper {

	/**
	 * Splits a list into an array with the given amount of indexes. <br>
	 * Attempts to evenly distribute the amount of indexes inside each list.
	 * */
	public static <T> List<T>[] split(List<T> list, int amount) {
		List<T>[] lists = new List[amount];
		// Fill array
		for (int i = 0; i < lists.length; i++) {
			lists[i] = new CopyOnWriteArrayList<T>();
		}
		
		int sizes = (int) Math.floor(list.size() / amount);
		
		// Hotfix to prevent exceptions
		if (list.size() <= sizes) {
			return new List[] { list };
		}
		for (int i = 0; i < list.size(); i++) {
			int index = i == 0 ? 0 : (int) Math.floor(i / sizes);
			if (index < lists.length && index >= 0)
				lists[index].add(list.get(i));
		}
		return lists;
	}
	
}
