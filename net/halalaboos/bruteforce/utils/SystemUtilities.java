package net.halalaboos.bruteforce.utils;

import java.io.File;

public interface SystemUtilities {

	public File getAppFolder();
	
	public void openFile(File file);
	
	public void openUrl(String url);
	
}
