package net.halalaboos.bruteforce.utils;

import net.halalaboos.bruteforce.Bruteforcer;
import net.halalaboos.bruteforce.Settings;
import net.halalaboos.bruteforce.event.EventManager;
import net.halalaboos.bruteforce.event.events.EventShutdown;

public class ShutdownHook extends Thread {

	@Override
	public void run() {
		Bruteforcer.getPluginManager().disablePlugins();
		EventManager.call(new EventShutdown());
		Bruteforcer.getSession().stop(); 
		Settings.save();
	}
	
}
