package net.halalaboos.bruteforce.utils;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public final class WindowHelper {

	
	public static File chooseFile(String chooseMessage) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.showDialog(null, chooseMessage);
		return fileChooser.getSelectedFile();
	}
	
	public static boolean displayQuestion(String title, String question) {
		return JOptionPane.showConfirmDialog(null, question, title, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
	}

}
