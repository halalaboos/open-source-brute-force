package net.halalaboos.bruteforce.utils.os;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import net.halalaboos.bruteforce.utils.SystemUtilities;

public final class MacUtils implements SystemUtilities {

	private static final File appData = new File(System.getProperty("user.home") + File.separator + "Library" + File.separator + "Application Support");
	
	@Override
	public File getAppFolder() {
		return appData;
	}

	@Override
	public void openFile(File file) {
		try {
			Desktop.getDesktop().open(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void openUrl(String url) {
		try {
			Desktop.getDesktop().browse(new URI(url));
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}

}
