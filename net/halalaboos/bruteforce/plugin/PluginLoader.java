package net.halalaboos.bruteforce.plugin;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import net.halalaboos.bruteforce.Bruteforcer;
import net.halalaboos.bruteforce.utils.reflection.ReflectionHelper;

public class PluginLoader {
	
	private static final String CONFIG_FILE_NAME = "config.cfg";

	private final PluginManager manager;
		
	private File pluginDir = null;
	
	public PluginLoader(PluginManager manager) {
		this.manager = manager;
	}
	
	public void setup() {
		getPluginDir();
	}
	
	public void loadPlugins() {
		File[] files = pluginDir.listFiles();
		for (File file : files) {
			if (file.exists() && !file.isDirectory()) {
				try {
					Plugin plugin = loadPlugin(file);
					if (plugin != null) {
						manager.registerPlugin(plugin);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public Plugin loadPlugin(File file) throws Exception {
        String classPath = null;
		JarFile jarFile = new JarFile(file);
        JarEntry entry;
        Enumeration<JarEntry> entries = jarFile.entries();

        while (entries.hasMoreElements()) {
            entry = entries.nextElement();
            if (entry.isDirectory())
                continue;
            if (entry.getName().equalsIgnoreCase(CONFIG_FILE_NAME)) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(jarFile.getInputStream(entry)));
                for (String line = null; (line = reader.readLine()) != null; ) {
                    if (line.startsWith("#"))
                        continue;
                    else if (line.toLowerCase().startsWith("classpath:")) {
                        classPath = line.substring("classpath:".length()).replaceAll(" ", "");
                    }
                }
                reader.close();
            }
        }
        jarFile.close();
        return ReflectionHelper.<Plugin>loadInstance(file, classPath, (Object[]) null);
	}
	

	public File getPluginDir() {
		if (pluginDir == null) {
			pluginDir = new File(Bruteforcer.getDir(), "plugins");
			if (!pluginDir.exists()) {
				pluginDir.mkdirs();
			}
		}
		return pluginDir;
	}
}
