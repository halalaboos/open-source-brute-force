package net.halalaboos.bruteforce.plugin;

import net.halalaboos.bruteforce.plugin.data.WorkerData;
import net.halalaboos.bruteforce.plugin.manager.DataManager;
import net.halalaboos.bruteforce.plugin.manager.OptionsManager;
import net.halalaboos.bruteforce.plugin.manager.ProxyManager;

public abstract class DefaultPlugin <T extends WorkerData> implements Plugin <T> {
	
	private boolean enabled = false;
	
	private final PluginData data;
		
	private OptionsManager optionsManager = new OptionsManager(this);
	
	private ProxyManager proxyManager = new ProxyManager();
	
	private DataManager dataManager = new DataManager(this);
	
	public DefaultPlugin(PluginData data) {
		this.data = data;
	}
	
	@Override
	public PluginData getData() {
		return data;
	}

	@Override
	public void enable() {
		if (!enabled) {
			this.onEnable();
			enabled = true;
		}
	}

	@Override
	public void disable() {
		if (enabled) {
			this.onDisable();
			enabled = false;
		}
	}

	@Override
	public void cleanup() {
	}


	@Override
	public void onEnable() {
	}

	@Override
	public void onDisable() {
	}

	@Override
	public OptionsManager getOptionsManager() {
		return optionsManager;
	}
	
	protected void setOptionsManager(OptionsManager optionsManager) {
		this.optionsManager = optionsManager;
	}
	
	@Override
	public ProxyManager getProxyManager() {
		return proxyManager;
	}

	protected void setProxyManager(ProxyManager proxyManager) {
		this.proxyManager = proxyManager;
	}

	@Override
	public DataManager getDataManager() {
		return dataManager;
	}

	protected void setDataManager(DataManager dataManager) {
		this.dataManager = dataManager;
	}

}
