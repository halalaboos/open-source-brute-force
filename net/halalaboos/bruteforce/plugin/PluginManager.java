package net.halalaboos.bruteforce.plugin;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.bruteforce.Bruteforcer;
import net.halalaboos.bruteforce.utils.Logger;

public final class PluginManager {
	
	private final List<Plugin> plugins = new CopyOnWriteArrayList<Plugin>();
	
	private final PluginLoader loader = new PluginLoader(this);
	
	private Plugin selectedPlugin = null;
	
	public PluginManager() {
		
	}
	
	public void setup() {
		plugins.clear();
		loader.setup();
	}
	
	public void enablePlugins() {
		for (Plugin plugin : plugins) {
			try {
				plugin.enable();
				Logger.log(plugin.getData().getName(), "enabled.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void disablePlugins() {
		for (Plugin plugin : plugins) {
			try {
				plugin.disable();
				Logger.log(plugin.getData().getName(), "disabled.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	

	public boolean hasSelectedPlugin() {
		return selectedPlugin != null;
	}
	
	public void selectPlugin(int index) {
		selectPlugin(get(index));
	}
	
	public void selectPlugin(Plugin plugin) {
		if (selectedPlugin != plugin && plugin != null) {
			if (selectedPlugin != null) {
				selectedPlugin.disable();
			}
			selectedPlugin = plugin;
			selectedPlugin.enable();
			Bruteforcer.getGui().getController().loadPluginOptions(selectedPlugin);
			Bruteforcer.getGui().getController().enableBegin();
			Bruteforcer.getSession().setPlugin(plugin);
			Logger.log(selectedPlugin.getData().getName(), "selected.");
		}
	}

	public Plugin getSelectedPlugin() {
		return selectedPlugin;
	}
	
	public void registerPlugin(Plugin plugin) {
		this.plugins.add(plugin);
	}

	public PluginLoader getLoader() {
		return loader;
	}

	public List<Plugin> getPlugins() {
		return plugins;
	}
	
	public List<PluginData> getPluginData() {
		List<PluginData> data = new CopyOnWriteArrayList<PluginData>();
		for (Plugin plugin : plugins)
			data.add(plugin.getData());
		return data;
	}

	public Plugin get(int index) {
		return plugins.get(index);
	}
	
	public int size() {
		return plugins.size();
	}
	
}
