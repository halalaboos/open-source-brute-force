package net.halalaboos.bruteforce.plugin;

public class PluginData {
	
	private final String name, author, version;
	
	private boolean requireProxies = true;
	
	public PluginData(String name, String author, String version) {
		this.name = name;
		this.author = author;
		this.version = version;
	}
	
	public String getName() {
		return name;
	}
	
	public String getAuthor() {
		return author;
	}

	public String getVersion() {
		return version;
	}
	
	@Override
	public String toString() {
		return name + " (" + version + ")";
	}

	public boolean requiresProxies() {
		return requireProxies;
	}

	public void setRequireProxies(boolean requireProxies) {
		this.requireProxies = requireProxies;
	}
}
