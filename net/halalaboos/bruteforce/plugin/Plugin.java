package net.halalaboos.bruteforce.plugin;

import java.net.Proxy;

import net.halalaboos.bruteforce.plugin.data.WorkerData;
import net.halalaboos.bruteforce.plugin.manager.DataManager;
import net.halalaboos.bruteforce.plugin.manager.OptionsManager;
import net.halalaboos.bruteforce.plugin.manager.ProxyManager;

/**
 * Basic interface for all plugins loaded. <br>
 * I'd recommend extending the DefaultPlugin class.
 * */
public interface Plugin <T extends WorkerData> {

	/**
	 * Invoked wehen enabled.
	 * */
	void onEnable();
	
	/**
	 * Invoked when disabled.
	 * */
	void onDisable();
	
	/**
	 * Information about the plugin.
	 * */
	public PluginData getData();
	
	/**
	 * Invoked to enable.
	 * */
	void enable();
	
	/**
	 * Invoked to disable.
	 * */
	void disable();
	
	/**
	 * Worker function.
	 * @return status code.
	 * */
	int doWork(T data) throws Exception;
		
	/**
	 * User-readable message reflecting the status.
	 * */
	String getStatusMessage(int status);
	
	/**
	 * Used to determine if the current data is sufficient enough to continue to the next set of data or retry with another proxy.
	 * */
	boolean shouldContinue(int status);
	
	/**
	 * Used to determine if the data should be added to the working data list.
	 * */
	boolean shouldAddToWorking(T data, int status);

	/**
	 * Used to generate the options window pane for the plugin.
	 * */
	OptionsManager getOptionsManager();
	
	/**
	 * Used to manage.. well.. proxies.
	 * */
	ProxyManager getProxyManager();
	
	/**
	 * Holds all working and normal data.
	 * */
	DataManager getDataManager();
	
	/**
	 * Loads the data from a file.
	 * */

	void cleanup();
	
}
