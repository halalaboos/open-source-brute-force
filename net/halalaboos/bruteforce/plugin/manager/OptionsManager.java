package net.halalaboos.bruteforce.plugin.manager;

import net.halalaboos.bruteforce.plugin.Plugin;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;

public class OptionsManager {

	protected final Plugin plugin;
	
	public OptionsManager(Plugin plugin) {
		this.plugin = plugin;
	}
	
	public Node generateOptions(ScrollPane scrollPane) {
		return new GridPane();
	}
	
}
