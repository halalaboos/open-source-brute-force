package net.halalaboos.bruteforce.plugin.manager;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.bruteforce.Settings;
import net.halalaboos.bruteforce.utils.NumberHelper;
import net.halalaboos.io.FileUtils;

public class ProxyManager {

	private final List<Proxy> proxies = new CopyOnWriteArrayList<Proxy>();
	
	private int proxyIndex = 0;
	

	public List<Proxy> loadProxies(File file) {
		return loadProxies(Proxy.Type.HTTP, file);
	}
	
	public List<Proxy> loadProxies(Proxy.Type type, File file) {
		List<String> lines = FileUtils.readFile(file);
		for (String line : lines) {
			Proxy proxy = parseProxy(type, line);
			if (proxy != null) {
				proxies.add(proxy);
			}
		}
		return proxies;
	}
	
	public void clearProxies() {
		proxies.clear();
	}
	
	public void resetProxyIndex() {
		proxyIndex = 0;
	}
	
	private Proxy parseProxy(Proxy.Type type, String line) {
		int port = 80;
		String address = line;
		if (line.contains(":")) {
			String portText = line.substring(line.indexOf(":") + 1).replaceAll(" ", "");
			if (NumberHelper.isInteger(portText)) {
				port = Integer.parseInt(portText);
				address = line.substring(0, line.indexOf(":")).replaceAll(" ", "");
			}
		}
		return new Proxy(type, new InetSocketAddress(address, port));
	}
	
	public void onFailedProxy(Proxy proxy) {
		proxies.remove(proxy);
	}
	
	public Proxy nextProxy() throws Exception {
		if (Settings.isRandomProxy()) {
			return proxies.get((int) (Math.random() * proxies.size()));
		} else {
			if (proxies.size() <= 0) {
				if (Settings.useHome())
					return Proxy.NO_PROXY;
				throw new Exception("Out of proxies");				
			}
			if (proxyIndex >= proxies.size()) {
				proxyIndex = 0;
				return nextProxy();
			} else {
				Proxy proxy = proxies.get(proxyIndex);
				proxyIndex++;
				return proxy;
			}
		}
	}
	
}
