package net.halalaboos.bruteforce.plugin.manager;

import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.bruteforce.Bruteforcer;
import net.halalaboos.bruteforce.Settings;
import net.halalaboos.bruteforce.io.ErrorHandler;
import net.halalaboos.bruteforce.plugin.Plugin;
import net.halalaboos.bruteforce.plugin.data.AccountData;
import net.halalaboos.bruteforce.plugin.data.WorkerData;
import net.halalaboos.bruteforce.utils.Logger;
import net.halalaboos.bruteforce.utils.WindowHelper;
import net.halalaboos.io.FileUtils;

public class DataManager <T extends WorkerData> {
	
	private List<T> data = new CopyOnWriteArrayList<T>();

    private List<T> working = new CopyOnWriteArrayList<T>();
   
    private final Plugin plugin;
    
    public DataManager(Plugin plugin) {
    	this.plugin = plugin;
    }
    
    public List<T> getWorking() {
		return working;
	}

	public void addData(T data) {
		this.data.add(data);
	}
	
	public void removeData(T data) {
		this.data.remove(data);
	}
	
	public void clearData() {
		data.clear();
	}
	
	public void saveData() {
		if (working.size() > 0) {
			if (WindowHelper.displayQuestion("Save", "Do you want to save the working data?")) {
				try {
					writeFile(Settings.getWorkingFile(), working);
					Bruteforcer.getSysUtils().openFile(Settings.getWorkingFile().getParentFile());
				} catch (Exception e) {
					e.printStackTrace();
					ErrorHandler.handleDataSaveError(e);
				}
			}
		}
	}

	public void addWorking(T data) {
		Logger.log("Working: " + data.toString());
		working.add(data);
		Bruteforcer.getGui().getController().log(data.toString());
	}
	
	public void loadData(File file) {
		try {
			data = readFile(file);
		} catch (Exception e) {
			Logger.logErr(e);
			ErrorHandler.handleDataReadError(e);
			return;
		}
	}

	protected List<T> readFile(File file) {
		List list = new CopyOnWriteArrayList();
		List<String> accounts = FileUtils.readFile(file);
		for (int i = 0; i < accounts.size(); i++) {
			String line = (String) accounts.get(i);
			if (line.contains(":")) {
				String username = line.substring(0, line.indexOf(":"));
				String password = line.substring(line.indexOf(":") + 1);

				list.add(new AccountData(username, password));
			}
		}
		return list;
	}

	public void writeFile(File file, List<T> list) throws Exception {
		if (!file.exists()) file.createNewFile();
		List<String> tempList = new CopyOnWriteArrayList<String>(); 
		for (T data : list) {
			tempList.add(data.toString());
		}
		FileUtils.writeFile(file, tempList);
	}
	
	public List<T> getData() {
		return data;
	}
	
}
