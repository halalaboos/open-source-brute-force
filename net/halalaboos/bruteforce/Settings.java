package net.halalaboos.bruteforce;

import java.io.File;
import java.io.IOException;

import net.halalaboos.bruteforce.plugin.Plugin;
import net.halalaboos.io.config.DefaultConfig;

public final class Settings {

	private static final DefaultConfig config = new DefaultConfig("Config", getConfigFile());
	
	private static boolean randomProxy = false;
		
	private static boolean useHome = false;
	
	private static float delay = 0F;
	
	private static File workingFile;
	
	private static int pluginId = -1;
	
	private static int threads = 1;
	
	private Settings() {
		
	}
	
	public static boolean isRandomProxy() {
		return randomProxy;
	}
	
	public static void setRandomProxy(boolean randomProxy) {
		Settings.randomProxy = randomProxy;
		config.put("Random Proxy", randomProxy);
	}

	public static boolean useHome() {
		return useHome;
	}

	public static void setUseHome(boolean useHome) {
		Settings.useHome = useHome;
		config.put("Use Home", useHome);
	}

	public static float getDelay() {
		return delay;
	}

	public static void setDelay(float delay) {
		Settings.delay = delay;
		config.put("Login Delay", delay);

	}

	public static int getPluginId() {
		return pluginId;
	}

	public static void setPluginId(int pluginId) {
		Settings.pluginId = pluginId;
		Settings.updatePlugin();
		config.put("Plugin ID", pluginId);
	}

	public static int getThreads() {
		return threads;
	}

	public static void setThreads(int threads) {
		Settings.threads = threads;
		config.put("Threads", threads);
	}

	public static File getWorkingFile() {
		if (!workingFile.exists()) {
			try {
				workingFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return workingFile;
	}

	public static void setWorkingFile(File workingFile) {
		Settings.workingFile = workingFile;
		config.put("Working File", workingFile.getAbsolutePath());
	}
	
	public static void setupDefaults() {
		setWorkingFile(new File(Bruteforcer.getDir(), "Working.txt"));
		setPluginId(-1);
		setThreads(1);
		setUseHome(false);
		setRandomProxy(false);
	}
	
	public static void save() {
		config.save();
	}
	
	public static void load() {
		config.load();
		Settings.randomProxy = config.getBoolean("Random Proxy");
		Settings.useHome = config.getBoolean("Use Home");
		Settings.delay = config.getFloat("Login Delay");
		Settings.workingFile = new File(config.get("Working File"));
		Settings.pluginId = config.getInt("Plugin ID");
		Settings.threads = config.getInt("Threads");
		updatePlugin();
	}
	
	
	public static void updatePlugin() {
		if (Settings.getPluginId() > -1 && Settings.getPluginId() < Bruteforcer.getPluginManager().size()) {
			Plugin plugin = Bruteforcer.getPluginManager().get(Settings.getPluginId());
			if (plugin != null) {
				Bruteforcer.getPluginManager().selectPlugin(plugin);
			}
		}
	}
	
	
	public static File getConfigFile() {
		File configFile = new File(Bruteforcer.getDir(), "config.cfg");
		if (!configFile.exists()) {
			try {
				configFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return configFile;
	}
	
	public static DefaultConfig getConfig() {
		return config;
	}
}
