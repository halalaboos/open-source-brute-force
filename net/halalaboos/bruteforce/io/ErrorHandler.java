package net.halalaboos.bruteforce.io;

import javax.swing.JOptionPane;

import net.halalaboos.bruteforce.utils.Logger;

public final class ErrorHandler {

	public static void handleDataReadError(Exception e) {
		e.printStackTrace();
		Logger.logErr(e);
		String message = e.getMessage();
		if (e.getMessage() == null) {
			message = "Something was null!";
		}
		JOptionPane.showMessageDialog(null, message, "Data read Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public static void handleDataSaveError(Exception e) {
		e.printStackTrace();
		Logger.logErr(e);
		String message = e.getMessage();
		if (e.getMessage() == null) {
			message = "Something was null!";
		}
		JOptionPane.showMessageDialog(null, message, "Data save Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public static void handleFatalWorkError(Exception e) {
		e.printStackTrace();
		Logger.logErr(e);
		String message = e.getMessage();
		if (e.getMessage() == null) {
			message = "Something was null!";
		}
		JOptionPane.showMessageDialog(null, message, "Worker Error", JOptionPane.ERROR_MESSAGE);
	}
}
