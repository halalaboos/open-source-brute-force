package net.halalaboos.bruteforce.handling;

import java.util.List;

import net.halalaboos.bruteforce.Settings;
import net.halalaboos.bruteforce.io.ErrorHandler;
import net.halalaboos.bruteforce.plugin.Plugin;
import net.halalaboos.bruteforce.plugin.data.WorkerData;
import net.halalaboos.bruteforce.plugin.manager.DataManager;
import net.halalaboos.bruteforce.plugin.manager.ProxyManager;
import net.halalaboos.bruteforce.utils.Logger;

public class WorkerThread extends Thread {
	
	private boolean running = false;
	    
    private final Session session;
    
    private final Plugin plugin;
    
    private final DataManager dataManager;
    
    private final ProxyManager proxyManager;
    
	private List<WorkerData> data;
	
	public WorkerThread(Session session, Plugin plugin, List<WorkerData> data) {
		this.session = session;
		this.data = data;
		this.plugin = plugin;
		this.dataManager = plugin.getDataManager();
		this.proxyManager = plugin.getProxyManager();
	}
	
	@Override
	public void run() {
		try {
			running = true;
			int position = 0;
			while (running) {
				int size = data.size();
				for (; position < size; ) {
					if (!running)
						break;					
					WorkerData data = this.data.get(position);
					if (data != null) {
						Logger.log("Working..");
						int status = plugin.doWork(data);
						
						Logger.log(plugin.getStatusMessage(status));
						if (plugin.shouldContinue(status)) {
							if (plugin.shouldAddToWorking(data, status))
								dataManager.addWorking(data);
							Logger.log("Next!");
							position++;
						} else
							Logger.log("Retrying..");
						
						Thread.sleep((long) Settings.getDelay());
					} else {
						position++;
					}
				}
				running = false;
			}
		} catch (Exception e) {
			Logger.logErr(e);
			ErrorHandler.handleFatalWorkError(e);
		} finally {
			session.onWorkerFinish(this);
		}
	}
	
	public void pause() {
		running = false;
	}
	
	public void begin() {
		running = true;
	}
	
}
