package net.halalaboos.bruteforce.handling;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import net.halalaboos.bruteforce.Bruteforcer;
import net.halalaboos.bruteforce.Settings;
import net.halalaboos.bruteforce.event.EventManager;
import net.halalaboos.bruteforce.event.events.EventWorkerFinish;
import net.halalaboos.bruteforce.gui.Gui;
import net.halalaboos.bruteforce.plugin.Plugin;
import net.halalaboos.bruteforce.plugin.data.WorkerData;
import net.halalaboos.bruteforce.plugin.manager.DataManager;
import net.halalaboos.bruteforce.plugin.manager.ProxyManager;
import net.halalaboos.bruteforce.utils.ListHelper;
import net.halalaboos.bruteforce.utils.Logger;
import net.halalaboos.bruteforce.utils.NumberHelper;

public class Session {

	private boolean running = false;
	
    private List<WorkerThread> workerThreads = new CopyOnWriteArrayList<WorkerThread>();
    
    private final Gui gui = Bruteforcer.getGui();
    
    private Plugin plugin;
    
    private DataManager dataManager;
    
    private ProxyManager proxyManager;
    
	public Session() {
		
	}
	
	public void setup() {	

	}
	
	
	public void stop() {
		if (running) {
			gui.getController().enableBegin();
			gui.getController().disablePause();
			gui.getController().enablePluginChanging();
			if (plugin.getData().requiresProxies())
				gui.getController().enableProxyField();

			Logger.log("Stopping!");
			running = false;
			for (WorkerThread workerThread : workerThreads) {
				workerThread.pause();
			}
			Logger.log(workerThreads.size() + " threads closed.");

			workerThreads.clear();
			plugin.getDataManager().saveData();
			plugin.cleanup();
		}
	
	}
	
	public void start() {
		if (!running) {
			gui.getController().disableBegin();
			gui.getController().enablePause();
			gui.getController().disablePluginChanging();
			if (plugin.getData().requiresProxies())
				gui.getController().disableProxyField();
			running = true;
			Logger.log("Starting!");
			try {
				List<WorkerData>[] datas = ListHelper.<WorkerData>split(dataManager.getData(), Settings.getThreads());
				for (int i = 0; i < datas.length; i++) {
					WorkerThread workerThread = new WorkerThread(this, plugin, datas[i]);
					workerThread.start();
					workerThreads.add(workerThread);
				}
				Logger.log(workerThreads.size() + " worker threads started.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void onWorkerFinish(WorkerThread workerThread) {
		Logger.log("Worker Finished!");
		if (this.workerThreads.contains(workerThread)) {
			this.workerThreads.remove(workerThread);
			if (workerThreads.size() <= 0) {
				stop();
			}
		}
		EventManager.call(new EventWorkerFinish(workerThread));
	}
	
	protected Proxy parseProxy(String line) {
		int port = 80;
		String address = line;
		if (line.contains(":")) {
			String portText = line.substring(line.indexOf(":") + 1).replaceAll(" ", "");
			if (NumberHelper.isInteger(portText)) {
				port = Integer.parseInt(portText);
				address = line.substring(0, line.indexOf(":")).replaceAll(" ", "");
			}
		}
		return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(address, port));
	}

	public Plugin getPlugin() {
		return plugin;
	}

	public void setPlugin(Plugin plugin) {
		this.plugin = plugin;
		this.dataManager = plugin.getDataManager();
		this.proxyManager = plugin.getProxyManager();
	}
}
