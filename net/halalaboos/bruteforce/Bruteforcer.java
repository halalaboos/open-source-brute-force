package net.halalaboos.bruteforce;

import java.io.File;

import net.halalaboos.bruteforce.factory.Factory;
import net.halalaboos.bruteforce.gui.Gui;
import net.halalaboos.bruteforce.handling.Session;
import net.halalaboos.bruteforce.plugin.Plugin;
import net.halalaboos.bruteforce.plugin.PluginManager;
import net.halalaboos.bruteforce.utils.Logger;
import net.halalaboos.bruteforce.utils.ShutdownHook;
import net.halalaboos.bruteforce.utils.SystemUtilities;

public final class Bruteforcer {

	private static final Bruteforcer instance = new Bruteforcer();
	
	private static final Gui gui = new Gui();
	
	private static final PluginManager pluginManager = new PluginManager();
	
	private static final SystemUtilities sysUtils = Factory.getSystemUtilities();
			
	private static final Session session = new Session();
		
	private static File bruteforcerDir = null;
	
	private Bruteforcer() {
	}
	
	public void setup() {
		pluginManager.setup();
		pluginManager.getLoader().loadPlugins();
		Runtime.getRuntime().addShutdownHook(new ShutdownHook());
		session.setup();
		gui.setup();
		Settings.setupDefaults();
		Settings.load();
	}
	
	public static File getDir() {
		if (bruteforcerDir == null) {
			bruteforcerDir = new File(sysUtils.getAppFolder(), ".bruteforcer");
			if (!bruteforcerDir.exists()) {
				bruteforcerDir.mkdirs();
			}
		}
		return bruteforcerDir;
	}

	public static Bruteforcer getInstance() {
		return instance;
	}

	public static Gui getGui() {
		return gui;
	}

	public static PluginManager getPluginManager() {
		return pluginManager;
	}

	public static SystemUtilities getSysUtils() {
		return sysUtils;
	}

	public static Session getSession() {
		return session;
	}
	
}
