package net.halalaboos.bruteforce.factory;

import net.halalaboos.bruteforce.utils.SystemUtilities;
import net.halalaboos.bruteforce.utils.os.LinuxUtils;
import net.halalaboos.bruteforce.utils.os.MacUtils;
import net.halalaboos.bruteforce.utils.os.WindowsUtils;

public class Factory {
	public static final int WINDOWS = 0, MAC = 1, LINUX = 2, NULL = -1;
	
	private static final int os = getOs();
	
	public static SystemUtilities getSystemUtilities() {
		switch (os) {
		case WINDOWS:
			return new WindowsUtils();
		case MAC:
			return new MacUtils();
		case LINUX:
			return new LinuxUtils();
		default:
			return null;
		}
	}
	
	private static int getOs() {
		String[] linux = { "linux", "unix" };
		String[] mac = { "osx", "mac" };
		String[] windows = { "windows", "win" };
		
		String os = System.getProperty("os.name").toLowerCase();
		for (String windows_ : windows) {
			if (os.contains(windows_)) {
				return 0;
			}
		}
		for (String mac_ : mac) {
			if (os.contains(mac_)) {
				return 1;
			}
		}
		for (String linux_ : linux) {
			if (os.contains(linux_)) {
				return 2;
			}
		}
		return -1;
	}
}
