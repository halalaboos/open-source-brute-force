package net.halalaboos.bruteforce.event;

import java.lang.reflect.Method;

public final class ListenerData {

	private final Listener listener;
	
	private final Method method;
	
	public ListenerData(Listener listener, Method method) {
		this.listener = listener;
		this.method = method;
	}

	public Listener getListener() {
		return listener;
	}

	public Method getMethod() {
		return method;
	}
	
}
