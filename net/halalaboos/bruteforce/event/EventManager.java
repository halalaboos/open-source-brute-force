package net.halalaboos.bruteforce.event;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public final class EventManager {

	private static final Map<Class<? extends Event>, List<ListenerData>> listeners = new ConcurrentHashMap<Class<? extends Event>, List<ListenerData>>();
	
	private EventManager() {
		
	}
	
	public static void registerListener(Listener listener) {
		List<ListenerData> listenerData = getData(listener);
		for (ListenerData data : listenerData) {
			Class[] parameters = data.getMethod().getParameterTypes();
			if (parameters.length == 1) {
				if (Event.class.isAssignableFrom(parameters[0])) {
					putOrUpdate(parameters[0]);
					listeners.get(parameters[0]).add(data);
				}
			}
		}
	}
	
	public static void registerListener(Class<? extends Event> class_, Listener listener) {
		putOrUpdate(class_);
		if (!containsListener(class_, listener)) {
			listeners.get(class_).addAll(getData(listener));
		}
	}

	public static void unregisterListener(Class<? extends Event> class_, Listener listener) {
		putOrUpdate(class_);
		List<ListenerData> listenerData = listeners.get(class_);
		for (ListenerData data : listenerData) {
			if (data.getListener() == listener) {
				listenerData.remove(data);
			}
		}
	}
	
	public static void unregisterListener(Listener listener) {
		for (Entry<Class<? extends Event>, List<ListenerData>> entry : listeners.entrySet()) {
			unregisterListener(entry.getKey(), listener);
		}
	}
	
	private static List<ListenerData> getData(Listener listener) {
		List<ListenerData> foundData = new ArrayList<ListenerData>();
		Method[] methods = listener.getClass().getMethods();
		for (Method method : methods) {
			if (method.isAnnotationPresent(EventHandler.class) && method.getParameterTypes().length == 1) {
				foundData.add(new ListenerData(listener, method));
			}
		}
		return foundData;
	}
	
	private static boolean containsListener(Class<? extends Event> class_, Listener listener) {
		List<ListenerData> listenerData = listeners.get(class_);
		for (ListenerData data : listenerData) {
			if (data.getListener() == listener) {
				return true;
			}
		}
		return false;
	}
	
	private static boolean containsListener(Listener listener) {
		for (Entry<Class<? extends Event>, List<ListenerData>> entry : listeners.entrySet()) {
			if (containsListener(entry.getKey(), listener)) {
				return true;
			}
		}
		return false;
	}
	
	private static void putOrUpdate(Class<? extends Event> class_) {
		if (!listeners.containsKey(class_)) {
			listeners.put(class_, new CopyOnWriteArrayList<ListenerData>());
		}
	}
	
	public static void call(Event event) {
		putOrUpdate(event.getClass());
		List<ListenerData> listenerData = listeners.get(event.getClass());
		for (ListenerData data : listenerData) {
			try {
				data.getMethod().invoke(data.getListener(), new Object[] { event });
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
