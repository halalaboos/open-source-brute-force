package net.halalaboos.bruteforce.event.events;

import net.halalaboos.bruteforce.event.Event;
import net.halalaboos.bruteforce.handling.WorkerThread;

public class EventWorkerFinish implements Event {

	private final WorkerThread workerThread;
	
	public EventWorkerFinish(WorkerThread workerThread) {
		this.workerThread = workerThread;
	}

	public WorkerThread getWorkerThread() {
		return workerThread;
	}
	
}
