import javafx.application.Application;
import javafx.stage.Stage;
import net.halalaboos.bruteforce.Bruteforcer;


public class Main extends Application {

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Bruteforcer.getInstance().setup();

		Bruteforcer.getGui().startDisplay(primaryStage);
	}

}
